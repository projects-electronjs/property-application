// ========== Variable et appelle framework electron ==========
const {BrowserWindow, app} = require('electron').remote;
const remote = require('electron').remote;
const {ipcRenderer} = require('electron');

// ========== Fonctions de création de la fenetre formulaire ==========
document.querySelector('#open-formulaire').addEventListener('click', createWindow)

function createWindow(){

    const winFormulaire = new BrowserWindow({
        width: 800,
        height: 600,
        maxWidth: 800,
        minWidth: 800,
        maxHeight: 600,
        minHeight: 600,
        webPreferences: {
            nodeIntegration: true
        },
        parent: remote.getCurrentWindow(),
        resizable: false
    });

    winFormulaire.loadFile(`app/formulaire/formulaire.html`);
    winFormulaire.show();

}

// ========== Fonctions d'ajout d'élément dans index.html ==========
ipcRenderer.on(`element-added`, showImmoInHTML);
app.whenReady().then(showImmoInHTML);

function showImmoInHTML(){

    let recoverDatasParse = JSON.parse(localStorage.getItem('elements'));

    const elementContainer = document.querySelector('tbody#liste-bien-immo');

    let cpt = 1;

    elementContainer.innerHTML = "";

    if (recoverDatasParse !== null && recoverDatasParse.length > 0){

        for(let recoverData of recoverDatasParse){
            let html = "";
            html = 
            `<tr>
                <td>${recoverData.name}</td>`;
            if(recoverData.rental === true){
                html += 
                `<td>Location</td>
                <td>${recoverData.city}</td>
                <td>${recoverData.size} m2</td>
                <td>${recoverData.price} €/mois</td>`
            } else{
                html += 
                `<td>Vente</td>
                <td>${recoverData.city}</td>
                <td>${recoverData.size} m2</td>
                <td>${recoverData.price} €</td>`
            }
            html +=
                `<td class="description">${recoverData.description}</td>`
            if(recoverData.linkPicture !== ""){
                html +=
                `<td>
                    <img src="${recoverData.linkPicture}" alt="${recoverData.name}" class="picture-focus">
                    <p style="font-size: 9px;text-align: center;">Cliquez pour agrandir</p>
                </td>`
            }else{
                html+=
                `<td>
                    <p style="font-size: 12px;text-align: center;">Pas de photo</p>
                </td>`
            }
            if(recoverData.booked === true){
                html += 
                `<td>
                    <p>Le bien est réservé</p>
                    <button id="btn-booked-${cpt}" class="btn btn btn-mini btn-default" disabled>Vous ne pouvez plus réserver le bien</button>
                </td>`
            } else{
                html +=
                `<td>
                    <p>Le bien n'est pas réservé</p>
                    <button id="btn-booked-${cpt}" class="btn-booked btn btn-mini btn-primary">Réserver le bien</button>
                </td>`
            }
            html +=            
                `<td>
                    <a id="btn-delete-${cpt}" class="btn-delete">Supprimer</a>
                </td>
            </tr>`

            elementContainer.innerHTML += html;
            cpt++;
            addEvenement()
        }
    }
}

// ========== Fonction d'ajout des événement à l'injection HTML (sinon addEventListener null) ==========
function addEvenement(){

    //Je selectionne tous mes boutons qui ont la classe btn-booked et je leur mets un événement de màj
    var allBtnBookable = document.querySelectorAll('button.btn-booked');
    for (let btnBookable of allBtnBookable){
        btnBookable.addEventListener(`click`, updateBookedLocalStorage)
    }

    //Je selectionne tous mes boutons qui ont la classe btn-delete et je leur mets un événement de suppression 
    var allBtnDelete = document.querySelectorAll('a.btn-delete');
    for (let btnDelete of allBtnDelete){
        btnDelete.addEventListener(`click`, deleteElemLocalStorage)
    }

    //Je selectionne toutes les images qui ont la classe picture-focus et je leur mets un évenement qui les ouvrent en grand
    var allPictures = document.querySelectorAll('img.picture-focus');
    for (let picture of allPictures){
        picture.addEventListener(`click`, focusPicture)
    }
}

// ========== Fonction de réservation de bien ==========
function updateBookedLocalStorage(){

    let arrayElements = JSON.parse(localStorage.getItem('elements'))

    //Je selectionne l'id du bouton qui a appelé la fonction et je ne garde que le nombre et je le transforme en int
    let idModif = parseInt((this.id.replace("btn-booked-", "")))

    //Je vais chercher la ligne de mon tableau à mettre à jour grace à l'id-1 car cpt commence à 1
    arrayElements[idModif-1].booked = true

    //Je modifie booked, je renvoie dans localStorage et je rappelle la fonction d'injection
    localStorage.setItem('elements', JSON.stringify(arrayElements));

    showImmoInHTML()
}

// ========== Fonctions de suppression de bien ==========
function deleteElemLocalStorage(){

    let arrayElements = JSON.parse(localStorage.getItem('elements'))

    //Comme la fonction au-dessus pour l'id
    let idModif = parseInt((this.id.replace("btn-delete-", "")))

    //Je supprime la ligne à l'indice idModif-1
    arrayElements.splice(idModif-1, 1)

    //Je renvoie dans localStorage et je rappelle la fonction d'injection
    localStorage.setItem('elements', JSON.stringify(arrayElements));

    showImmoInHTML()
}

// ========== Fonctions d'ouverture d'une fenetre avec photo ==========
function focusPicture(){

    const winPicture = new BrowserWindow({
        width: 800,
        height: 600,
        maxWidth: 800,
        minWidth: 800,
        maxHeight: 600,
        minHeight: 600,
        webPreferences: {
            nodeIntegration: true
        },
        resizable: false
    });
    
        winPicture.loadURL(this.src);

}